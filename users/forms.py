from typing import Any
from django.forms import ModelForm, FileInput, ImageField
from django import forms
from django.contrib.auth.forms import UserCreationForm
from .models import User,Profile

class UserCreationForm(UserCreationForm):
    class Meta:
        model = User
        fields = ['email','name','password1','password2']
    def __init__(self, *args: Any, **kwargs: Any) -> None:
        super().__init__(*args, **kwargs)
        self.fields['password1'].help_text = ''

class ProfileEditForm(ModelForm):
    picture =  ImageField(widget=FileInput)
    verification_method = forms.ChoiceField(choices=Profile.VerificationMethod.choices, widget=forms.Select(attrs={'class':'text-dark','label':'Verification Method'}))
    class  Meta:
        model = Profile
        fields = ['picture','cohort','phonenumber','cohort','verification_method']

class LoginForm(forms.Form):
    email = forms.CharField()
    password = forms.CharField(widget=forms.PasswordInput)