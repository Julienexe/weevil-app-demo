from django.urls import path
from . import views

app_name = "users"
urlpatterns=[
    path('login/',views.loginView,name='login'),
    #logout
    path("logout/",views.logoutView, name="logout"),
    #register
    path("register/",views.register, name="register"),
    #edit profile
    path('edit_profile/', views.edit_profile, name='edit_profile'),
    #view profile
    path('profile/', views.profileView, name='user-profile'),
    #cohort members
    path('cohort/',views.cohort_members, name='cohort_members'),
    #verify members
    path('add_member/<user_id>/', views.verify_member, name= 'add_member'), 
]