from django.shortcuts import render, redirect
from django.contrib.auth import login, logout, authenticate
from django.contrib import messages
from django.http import  Http404

from .models import User,Profile
from .forms import UserCreationForm, ProfileEditForm,LoginForm

def loginView(request):
    #code to login user in using  username and password
    if request.method == "POST": 
        form = LoginForm(data=request.POST)    
        if form.is_valid():
            email = form.cleaned_data['email']    
            password = form.cleaned_data['password']  
            user = authenticate(request, username=email, password=password)
            if user is not None:
                login(request, user)
                messages.success(request, f"You are now logged in as {user.username}")
                return redirect("weevils:home")
            else:
                messages.warning(request, f"login failed")
                return render(request, 'users/login.html', {'form':form})
    else:
        form = LoginForm()
        return render(request, 'users/login.html', {'form':form})

def logoutView(request):
    logout(request)
    return redirect("weevils:home")

def register(request):
    #page to register new users
    if  request.method == "POST":
        form = UserCreationForm(request.POST)
        if form.is_valid():
            user = form.save()
            login(request,user)
            return redirect('users:edit_profile')
    else:
        form = UserCreationForm()
    return render(request, 'users/register.html', {'form': form})

def profileView(request):
    profile = request.user.profile
    context = {
        'profile' : profile,
    }
    return  render(request,'users/profile.html',context)


def edit_profile(request):
    #page to edit user profiles
    if request.method == 'POST':
        form = ProfileEditForm(request.POST, request.FILES, instance=request.user.profile)
        if form.is_valid():
            if request.POST['example-radio'] == 'public':
                request.user.profile.private_profile = False
            else:
                request.user.profile.private_profile = True
            form.save()
            
            return redirect("weevils:home")
    else:
        form = ProfileEditForm(instance=request.user.profile)
    return render(request, 'users/edit_profile.html', {'form' : form})

def cohort_members(request):
    cohort = request.user.profile.cohort
    members = User.objects.filter(profile__cohort=cohort).order_by('username')
    context={'members':members}
    #verify cohort member when their verification link is active
    return render(request,'users/cohort_members.html',context)

def verify_member(request,user_id):
    """
    Verify a member of the current logged in user's cohort.
    Redirect back to cohort members page upon success or error.
    """
    try:
        user = User.objects.get(pk=user_id)
    except User.DoesNotExist:
        raise Http404("User does not exist.")
    
    if user != request.user and request.user.profile.cohort == user.profile.cohort:
        user.profile.verified = True
        user.profile.save()
        messages.success(request,"Member has been verified!")
    elif request.user == user:
        messages.error(request,"You cannot verify yourself.")
    else:
        messages.error(request,"You do not have permission to perform this action.")
    return redirect('users:cohort_members')