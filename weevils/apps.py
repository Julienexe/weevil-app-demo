from django.apps import AppConfig


class WeevilsConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'weevils'
