from django.urls import path
from . import views

app_name = "weevils"
urlpatterns=[
    path('',views.home,name='home'),
]