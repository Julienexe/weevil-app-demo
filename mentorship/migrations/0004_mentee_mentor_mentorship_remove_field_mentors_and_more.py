# Generated by Django 4.2.10 on 2024-04-28 09:38

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('users', '0005_rename_is_stealthy_profile_private_profile'),
        ('mentorship', '0003_field_image'),
    ]

    operations = [
        migrations.CreateModel(
            name='Mentee',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('bio', models.TextField()),
                ('image', models.ImageField(blank=True, null=True, upload_to='mentee-images/')),
            ],
        ),
        migrations.CreateModel(
            name='Mentor',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('bio', models.TextField()),
                ('experience', models.TextField()),
                ('availability', models.TextField()),
                ('image', models.ImageField(blank=True, null=True, upload_to='mentor-images/')),
            ],
        ),
        migrations.CreateModel(
            name='Mentorship',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('start_date', models.DateField()),
                ('end_date', models.DateField()),
                ('is_active', models.BooleanField(default=True)),
                ('mentee', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='mentorship.mentee')),
                ('mentor', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='mentorship.mentor')),
            ],
        ),
        migrations.RemoveField(
            model_name='field',
            name='mentors',
        ),
        migrations.CreateModel(
            name='Subfield',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=100)),
                ('field', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='subfields', to='mentorship.field')),
            ],
        ),
        migrations.CreateModel(
            name='Message',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('text', models.TextField()),
                ('date', models.DateTimeField(auto_now_add=True)),
                ('mentorship', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='mentorship.mentorship')),
                ('sender', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='users.profile')),
            ],
        ),
        migrations.AddField(
            model_name='mentor',
            name='fields',
            field=models.ManyToManyField(related_name='mentors', to='mentorship.subfield'),
        ),
        migrations.AddField(
            model_name='mentor',
            name='user',
            field=models.OneToOneField(on_delete=django.db.models.deletion.CASCADE, to='users.profile'),
        ),
        migrations.AddField(
            model_name='mentee',
            name='fields',
            field=models.ManyToManyField(related_name='mentees', to='mentorship.field'),
        ),
        migrations.AddField(
            model_name='mentee',
            name='user',
            field=models.OneToOneField(on_delete=django.db.models.deletion.CASCADE, to='users.profile'),
        ),
    ]
