from django.urls import path
from .views import *
app_name = "mentorship"

urlpatterns=[
    path('', landing, name='landing'),
    path('field/<int:field_id>/', field, name='field'),
]