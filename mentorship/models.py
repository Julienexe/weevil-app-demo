from django.db import models
from users.models import Profile

class Field(models.Model):
    name = models.CharField(max_length=100)
    image = models.ImageField(upload_to="field-images/", blank=True, null=True)
    
    def __str__(self):
        return self.name

class Subfield(models.Model):
    name = models.CharField(max_length=100)
    description = models.TextField(default="This field is about ...")
    field = models.ForeignKey(Field, on_delete=models.CASCADE, related_name="subfields")
    
    def __str__(self):
        return self.name

class Mentor(models.Model):
    user = models.OneToOneField(Profile, on_delete=models.CASCADE)
    fields = models.ManyToManyField(Subfield , related_name="mentors")
    bio = models.TextField()
    experience = models.TextField()
    availability = models.TextField()
    image = models.ImageField(upload_to="mentor-images/", blank=True, null=True)

    def __str__(self):
        return self.user.user.email
    
class Mentee(models.Model):
    user = models.OneToOneField(Profile, on_delete=models.CASCADE)
    fields = models.ManyToManyField(Field, related_name="mentees")
    bio = models.TextField()
    image = models.ImageField(upload_to="mentee-images/", blank=True, null=True)

    def __str__(self):
        return self.user.user.username

class Mentorship(models.Model):
    mentor = models.ForeignKey(Mentor, on_delete=models.CASCADE)
    mentee = models.ForeignKey(Mentee, on_delete=models.CASCADE)
    start_date = models.DateField()
    end_date = models.DateField()
    is_active = models.BooleanField(default=True)

    def __str__(self):
        return f"{self.mentor.user.user.username} - {self.mentee.user.user.username}"

class Message(models.Model):
    mentorship = models.ForeignKey(Mentorship, on_delete=models.CASCADE)
    sender = models.ForeignKey(Profile, on_delete=models.CASCADE)
    text = models.TextField()
    date = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return self.text[:70]


class Testimonial(models.Model):
    field = models.ForeignKey(Field, on_delete=models.CASCADE)
    text = models.TextField()
    author = models.ForeignKey(Profile, on_delete=models.CASCADE)

    def  __str__(self):
        return self.text[:70]
