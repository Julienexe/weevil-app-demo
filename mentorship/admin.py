from django.contrib import admin
from .models import *

admin.site.register(Field)
admin.site.register(Testimonial)
admin.site.register(Subfield)
admin.site.register(Mentor)
admin.site.register(Mentee)
admin.site.register(Mentorship)
admin.site.register(Message)
