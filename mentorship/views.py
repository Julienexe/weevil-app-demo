from django.shortcuts import render
from .models import *

def landing(request):
    fields = Field.objects.all()
    testimonials = Testimonial.objects.all()
    context = {
        'ments': fields,
        'testimonials': testimonials,
               }
    return render(request, 'landing.html',context)

def field(request, field_id):
    field = Field.objects.get(id=field_id)
    context = {
        'field': field,
    }
    return render(request, 'field.html', context)
