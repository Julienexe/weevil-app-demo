# weevil-app-demo




## Description
This is a demo for the weevil app that will be created on github(due to better python/django support )
It will include tests for the functionality and different ideas shared by the contributors and is written in python django



## Installation
This is simple, clone the repo, create a virtual environment for the repo, install everything in requirements.txt
```

python -m pip -r install requirements.txt

```


## Support
Reach me on my email, julianssonko@gmail.com

## Roadmap
Upon completion of this app, the source code will be used to create a RESTful API using Django Rest Framework, on github(again, due to better support for python django and easier hosting on railway)

## Contributing
Contributing is as easy as forking the project, commiting changes and opening a pull request


